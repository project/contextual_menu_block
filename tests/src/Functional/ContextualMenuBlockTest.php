<?php

declare(strict_types=1);

namespace Drupal\Tests\contextual_menu_block\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the contextual menu block.
 *
 * @group contextual_menu_block
 */
final class ContextualMenuBlockTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block',
    'node',
    'menu_ui',
    'contextual_menu_block',
  ];
  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';
  /**
   * A standard contextual menu block.
   *
   * @var \Drupal\block\Entity\Block
   */
  protected $block;
  /**
   * A contextual menu block configured to render on top-level pages.
   *
   * @var \Drupal\block\Entity\Block
   */
  protected $blockTopLevel;

  /**
   * Tests that the contextual menu block renders properly.
   */
  public function testContextualMenu() {
    $block_selector = "#block-{$this->block->id()}";
    $block_top_level_selector = "#block-{$this->blockTopLevel->id()}";
    // Ensure that the contextual menu is not rendered on top-level pages by
    // default.
    foreach (['one', 'two', 'three'] as $title) {
      $page = $this->drupalGetNodeByTitle($title);
      $this->drupalGet('/node/' . $page->id());
      $this->assertSession()->elementNotExists('css', $block_selector);
    }
    // Ensure that the contextual menu is rendered on top-level pages when
    // specifically configured to do so.
    foreach (['one', 'two'] as $title) {
      $page = $this->drupalGetNodeByTitle($title);
      $this->drupalGet('/node/' . $page->id());
      $this->assertSession()->elementExists('css', $block_top_level_selector);
    }
    // Assert that the block is not rendered on top-level pages if the page
    // does not have children, even if the block has been configured to render
    // on top-level pages.
    $page = $this->drupalGetNodeByTitle('three');
    $this->drupalGet('/node/' . $page->id());
    $this->assertSession()->elementNotExists('css', $block_selector);
    // Ensure that second and third level pages do render the contextual menu.
    $titles = [
      'one.one',
      'one.one.one',
      'one.one.two',
      'one.two',
      'two.one',
    ];
    foreach ($titles as $title) {
      $page = $this->drupalGetNodeByTitle($title);
      $this->drupalGet('/node/' . $page->id());
      $this->assertSession()->elementExists('css', '.layout-content');
    }
    // Ensure that a page with children renders menu items for its children and
    // does not render items for its parent or siblings.
    $page = $this->drupalGetNodeByTitle('one.one');
    $this->drupalGet('/node/' . $page->id());
    $this->assertSession()->linkExistsExact('one.one.one');
    $this->assertSession()->linkExistsExact('one.one.two');
    $this->assertSession()->linkNotExistsExact('one');
    $this->assertSession()->linkNotExistsExact('one.two');
    // Ensure that a page without children renders menu items for its parent and
    // siblings and does not render menu items for any other pages' children or
    // pages which are not in its ancestry.
    $page = $this->drupalGetNodeByTitle('one.two');
    $this->drupalGet('/node/' . $page->id());
    $this->assertSession()->linkExistsExact('one');
    $this->assertSession()->linkExistsExact('one.one');
    $this->assertSession()->linkNotExistsExact('one.one.one');
    $this->assertSession()->linkNotExistsExact('two.one');
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $admin_user = $this->drupalCreateUser([], NULL, TRUE);
    $this->drupalLogin($admin_user);

    $this->block = $this->drupalPlaceBlock('contextual_menu_block', ['menu_id' => 'main']);
    $this->blockTopLevel = $this->drupalPlaceBlock('contextual_menu_block', [
      'menu_id' => 'main',
      'render_on_top_level_items' => TRUE,
    ]);

    $this->drupalCreateContentType(['type' => 'page', 'name' => 'Basic page']);

    $this->createPageHierarchy([
      [
        'title' => 'one',
        'children' => [
          [
            'title' => 'one.one',
            'children' => [
              [
                'title' => 'one.one.one',
              ],
              [
                'title' => 'one.one.two',
              ],
            ],
          ],
          [
            'title' => 'one.two',
          ],
        ],
      ],
      [
        'title' => 'two',
        'children' => [
          [
            'title' => 'two.one',
          ],
        ],
      ],
      [
        'title' => 'three',
      ],
    ]);
  }

  /**
   * Helper to create a new page and menu item and do the same for its children.
   *
   * @param array $tree
   *   A multi-dimensional array of menu element templates containing a page
   *   title and the pages children, if any.
   * @param string|null $parent_title
   *   Internal use only. See inline comment below.
   */
  protected function createPageHierarchy(array $tree, ?string $parent_title = NULL): void {
    foreach ($tree as $element) {
      // Create a new page with the given title and add a menu item for it.
      $this->drupalGet('/node/add/page');
      $form_values = [
        'title[0][value]' => $element['title'],
        'menu[enabled]' => 1,
        'menu[title]' => $element['title'],
      ];

      // If the tree has a parent, look up the page's node ID by its title,
      // then use that to load the corresponding menu item in order to set
      // up the tree as children of that parent page.
      if ($parent_title) {
        $page_id = $this->drupalGetNodeByTitle($parent_title)->id();
        $menu_storage = $this->container->get('plugin.manager.menu.link');
        $route_params = ['node' => $page_id];
        $parent_menu_item = current($menu_storage->loadLinksByRoute('entity.node.canonical', $route_params));
        $form_values['menu[menu_parent]'] = "main:{$parent_menu_item->getPluginId()}";
      }

      // Submit the node add form.
      $this->submitForm($form_values, 'Save');
      // If the current element has children, then recursively to create pages
      // and links for them.
      if (!empty($element['children'])) {
        $this->createPageHierarchy($element['children'], $element['title']);
      }
    }
  }

}
