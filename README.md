## Contextual Menu Block

Contextual Menu Block module provides a menu block that site builders can place so that
end users can easily navigate a page hierarchy.

This module does not support variable starting levels and/or depth or
other configuration options. It provides links for the immediate parent and
sibling menu items of the active menu item, or it provides links to its immediate
children. It does not provide parent of parent links or child of child links.

Most sites will prefer to use this module in conjunction with the
[Menu Breadcrumb](https://www.drupal.org/project/menu_breadcrumb) module, which will afford visitors with a link to navigate up
the menu hierarchy when this block does not render a parent link (i.e.
when the active item has children).

For a full description of the module, visit the
[project page](https://www.drupal.org/project/admin_toolbar).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/search/admin_toolbar).

## Table of contents

- Requirements
- Recommended modules
- Installation
- Configuration

## Requirements

This module does not require any additional modules.

## Recommended modules

[Menu Breadcrumb](https://www.drupal.org/project/menu_breadcrumb):
Most sites will prefer to use this module in conjunction with the
Menu Breadcrumb module, which will afford visitors with a link to navigate up
the menu hierarchy when this block does not render a parent link (i.e. when
the active item has children).

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

Place the "Contextual menu" block using the layout tool of your choice.
Then select which menu you want to be used as a contextual menu.
