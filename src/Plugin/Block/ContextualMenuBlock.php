<?php

declare(strict_types=1);

namespace Drupal\contextual_menu_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Menu\MenuActiveTrailInterface;
use Drupal\Core\Menu\MenuLinkInterface;
use Drupal\Core\Menu\MenuLinkTreeElement;
use Drupal\Core\Menu\MenuLinkTreeInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\system\Entity\Menu;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a contextual menu block using a configured menu.
 *
 * In concert with page breadcrumbs and a main menu, an end-user will be able
 * to use this menu block to navigate up or down exactly one level within the
 * site's page hierarchy or "laterally" within the end-user's current level.
 *
 * @Block(
 *   id = "contextual_menu_block",
 *   admin_label = @Translation("Contextual menu"),
 *   category = @Translation("Navigation"),
 *   forms = {
 *     "settings_tray" = "\Drupal\system\Form\SystemMenuOffCanvasForm",
 *   },
 * )
 */
class ContextualMenuBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The menu link tree service.
   *
   * @var \Drupal\Core\Menu\MenuTreeStorageInterface
   */
  protected $menuStorage;

  /**
   * The menu link tree service.
   *
   * @var \Drupal\Core\Menu\MenuLinkTreeInterface
   */
  protected $menuTree;

  /**
   * The active menu trail service.
   *
   * @var \Drupal\Core\Menu\MenuActiveTrailInterface
   */
  protected $menuActiveTrail;

  /**
   * Constructs a new SystemMenuBlock.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $menu_storage
   *   The menu storage.
   * @param \Drupal\Core\Menu\MenuLinkTreeInterface $menu_tree
   *   The menu tree service.
   * @param \Drupal\Core\Menu\MenuActiveTrailInterface $menu_active_trail
   *   The active menu trail service.
   */
  public function __construct(array $configuration, string $plugin_id, array $plugin_definition, EntityStorageInterface $menu_storage, MenuLinkTreeInterface $menu_tree, MenuActiveTrailInterface $menu_active_trail) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->menuStorage = $menu_storage;
    $this->menuTree = $menu_tree;
    $this->menuActiveTrail = $menu_active_trail;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('menu'),
      $container->get('menu.link_tree'),
      $container->get('menu.active_trail'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $menus = $this->menuStorage->loadMultiple();
    $form['menu_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Menu'),
    ];
    foreach ($menus as $menu) {
      $form['menu_id']['#options'][$menu->id()] = $menu->label();
    }
    $form['menu_id']['#default_value'] = $this->configuration['menu_id'] ?? NULL;
    $form['top_level'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable on top-level pages'),
      '#description' => $this->t('If checked, this block will be displayed when the active menu item is located at the top level of the menu hierarchy. If unchecked, this block will be omitted from the page.'),
      '#default_value' => $this->configuration['render_on_top_level_items'] ?? FALSE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['menu_id'] = $form_state->getValue('menu_id');
    $this->configuration['render_on_top_level_items'] = $form_state->getValue('top_level');
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $menu_id = $this->configuration['menu_id'];
    $build = [];
    $cacheability = new CacheableMetadata();
    $cacheability->addCacheContexts(['route']);
    $cacheability->addCacheableDependency(Menu::load($menu_id));
    $cacheability->applyTo($build);
    $active_link = $this->menuActiveTrail->getActiveLink($menu_id);
    if (!$active_link || empty($this->configuration['render_on_top_level_items']) && empty($active_link->getParent())) {
      return $build;
    }
    $parameters = $this->menuTree->getCurrentRouteMenuTreeParameters($menu_id);
    $parameters->onlyEnabledLinks();
    $tree = $this->menuTree->load($menu_id, $parameters);
    $manipulators = [
      [
        'callable' => [self::class, 'filterTree'],
        'args' => [$active_link->getPluginId()],
      ],
      ['callable' => 'menu.default_tree_manipulators:checkAccess'],
      ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
    ];
    $tree = $this->menuTree->transform($tree, $manipulators);
    if (empty($tree)) {
      return $build;
    }
    $build = $this->menuTree->build($tree);
    array_walk($build['#items'], [static::class, 'removeNoLinkItem']);
    array_walk($build['#items'], [static::class, 'unlinkActiveItem'], $active_link);
    $cacheability->applyTo($build);
    return $build;
  }

  /**
   * Array walk callback which locates removes <nolink> menu items.
   */
  protected static function removeNoLinkItem(array &$item, $index) {
    foreach ($item['below'] as $child) {
      if ($child['url']->isRouted() && $child['url']->getRouteName() === '<nolink>') {
        $plugin_id = $child['original_link']->getPluginId();
        unset($item['below'][$plugin_id]);
      }
    }
  }

  /**
   * Array walk callback which locates and unlinks the active menu item.
   */
  protected static function unlinkActiveItem(array &$item, $index, MenuLinkInterface $active_link) {
    if ($item['in_active_trail'] ?? FALSE) {
      assert($item['original_link'] instanceof MenuLinkInterface);
      if ($item['original_link']->getPluginId() === $active_link->getPluginId()) {
        assert($item['url'] instanceof Url);
        $item['url'] = Url::fromRoute('<nolink>', [], [
          'set_active_class' => TRUE,
          'attributes' => [
            'class' => ['is-active'],
          ],
        ]);
      }
      if ($item['below'] ?? FALSE) {
        array_walk($item['below'], [static::class, 'unlinkActiveItem'], $active_link);
      }
    }
  }

  /**
   * Filters a menu tree relative to the active element.
   *
   * Implements the following logic:
   *
   * 1. If the active element has no parents, then an empty array is returned.
   *    Otherwise,
   * 2. If the active element has no children, the root of the returned tree is
   *    the active element's parent and the root's children are the active
   *    element and its siblings. Otherwise,
   * 3. If the active element does have children, the root of the returned tree
   *    is the active element and the root's children are the immediate
   *    offspring of the active element.
   *
   * @param \Drupal\Core\Menu\MenuLinkTreeElement[] $tree
   *   The tree to be filtered.
   * @param string $active_id
   *   The menu element ID of the active tree element.
   * @param \Drupal\Core\Menu\MenuLinkTreeElement|null $parent
   *   Internal use only. Do not pass a parent element. See inline comments
   *   within method body.
   *
   * @return \Drupal\Core\Menu\MenuLinkTreeElement[]
   *   The filtered tree.
   */
  public static function filterTree(array $tree, string $active_id, ?MenuLinkTreeElement $parent = NULL): array {
    foreach ($tree as $element) {
      if ($element->inActiveTrail) {
        // The active element must be a child of the current element if the
        // current element is in the active trail, but it is not the active
        // element itself.
        if ($element->link->getPluginId() !== $active_id) {
          // Therefore, recurse into the subtree and pass the current element as
          // the parent of the subtree.
          return self::filterTree($element->subtree, $active_id, $element);
        }
        if ($element->hasChildren) {
          // Remove grandchildren.
          $element->subtree = self::removeSubtrees($element->subtree);
          return [$element];
        }
        // Top-level elements without children should not render.
        if (is_null($parent)) {
          return [];
        }
        // If the element has no children, the menu tree should have the
        // current element's parent at its root and all the current
        // element's siblings should be children of the root element and
        // their children should not be rendered.
        $parent->subtree = self::removeSubtrees($tree);
        return [$parent];
      }
    }
    // None of the tree elements were in the active trail, indicating that the
    // current page does not have an element anywhere in the navigation.
    return [];
  }

  /**
   * Removes subtrees from the list of given menu link tree elements.
   *
   * @param \Drupal\Core\Menu\MenuLinkTreeElement[] $tree
   *   An array of menu elements from which any child elements should be
   *   removed.
   *
   * @return array
   *   A one-level deep menu link element tree.
   */
  protected static function removeSubtrees(array $tree): array {
    return array_map(function (MenuLinkTreeElement $child): MenuLinkTreeElement {
      $child->hasChildren = FALSE;
      $child->subtree = [];
      return $child;
    }, $tree);
  }

}
